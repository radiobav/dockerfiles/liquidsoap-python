FROM phasecorex/liquidsoap:latest

RUN apt-get update && apt-get upgrade -y && apt-get install python3 python3-pip -y
RUN python3 -m pip install --upgrade pip
RUN pip3 install pytz
LABEL maintainer="Marvin Weiler <marvin@m2rocks.de>"
